This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

Plot of the movie:

* Rich kid buys his way into college. Takes a monkey. (0-5 mins)

* The rich kid never intended to go to college, but rather used that
  as a reason to get out of home. He intends to go to Boston and work. (5-25)

* The monkey is sent to college instead. Lots of hijinks. (25-40)

* rich kid works in a bar, the monkey befriends a group of nerds and
  forms a band. they play at the rich kid's bar. (40-)

* rich kid's bar owner dies. the police don't do much to
  investigate. the monkey and his nerd friends sleuth the fuck out of
  it and solve the murder. (50-)

* they play a benefit gig at the bar, which the rich kid runs. (80-)

* movie ends. (90)